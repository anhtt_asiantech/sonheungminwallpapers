package com.anhttvn.sonheungminwallpapers.util;

public class ConfigUtil {
    public static final String FOLDER_DOWNLOAD = "SON_HEUNG_MIN";
    public static final String FOLDER_ASSETS = "wallpaper";
    /**
     * api
     */
    public static String KEY_NOTIFICATION = "Notification";

    public static String KEY_WALLPAPER = "Wallpaper_v2";
    public static String INFORMATION ="Information";

//  Config Database

    public static  final String DATABASE = "SonheungminWallpaper.db";
    public static  final int VERSION = 4;
}
